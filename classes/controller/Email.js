const { Controller } = require('@kohanajs/core-mvc');
const {ControllerMixinMultipartForm} = require("@kohanajs/mod-form");
const { ControllerMixinDatabase, ControllerMixinMime, KohanaJS, ORM } = require('kohanajs');

const Mail = ORM.require('Mail');
const Unsubscribe = ORM.require('Unsubscribe');
const HelperMail = KohanaJS.require('helper/Mail');

/**
 * Controller Email
 */
class ControllerEmail extends Controller {
  static mixins = [...Controller.mixins,
    ControllerMixinMime,
    ControllerMixinDatabase
  ];

  constructor(request) {
    super(request);

    this.state.get(ControllerMixinDatabase.DATABASE_MAP).set('mail', KohanaJS.config.mail.dbMail);
  }

  async action_view() {
    const database = this.state.get(ControllerMixinDatabase.DATABASES).get('mail');
    const mail = await ORM.factory(Mail, this.request.params.id, { database });
    this.body = await new HelperMail().read(mail.html_template, JSON.parse(mail.tokens));
  }

  async action_unsubscribe(){

  }

  async action_unsubscribe_post(){
    const database = this.state.get(ControllerMixinDatabase.DATABASES).get('mail');
    const $_POST = this.state.get(ControllerMixinMultipartForm.POST_DATA);
    const $_GET = this.state.get(ControllerMixinMultipartForm.GET_DATA);

    const mail = await ORM.factory(Mail, $_POST['message_id'], { database });
    const recipients = new Set(mail.recipient.replaceAll(' ', '').split(','));
    if(recipients.has($_POST['email'])){
      await ORM.create(Unsubscribe, {database});
    }

    if($_POST['destination'] || $_GET['cp']){
      await this.redirect($_POST['destination'] || $_GET['cp']);
    }else{
      await this.redirect(`/${this.language}/unsubscribe/thank-you`);
    }
  }

  async action_unsubscribe_thankyou(){

  }
}

module.exports = ControllerEmail;
